package com.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

/**
 * Unit test for simple App.
 */
public class testParameters 
{
    
    exam1 exam;
    @Before
    public void beforeTest(){
        exam=new exam1();
    }

    @Test
    public void testExam(){
        assertFalse(exam.exam(0));
        assertFalse(exam.exam(1));
        assertFalse(exam.exam(8));
        assertTrue(exam.exam(7));
        assertTrue(exam.exam(2));
    }
    @Test
    public void testGetRestZero()
    {
        assertTrue(exam.getRestZero(1,1));
    }
    @Test
    public void testOneHalf()
    {

        assertEquals(1 , exam.oneHalf(2));
    }
}
